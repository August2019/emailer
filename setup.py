from setuptools import setup
setup(
   name='emailer',
   version='1.0.0',
   author="Shrey Jaradi",
   author_email='jaradishrey@gmail.com',
   packages=['emailer'],
   url='https://gitlab.com/August2019/emailer',
   license='LICENSE.txt',
   description='This package is use to send mails',
   long_description=open('README.md').read(),
   install_requires= ["pyyaml",
                      "progressbar",
                      ],
          
   include_package_data=True,
   entry_points={
        'console_scripts': [
            'emailer = emailer:main',
        ],
    }
   
)
