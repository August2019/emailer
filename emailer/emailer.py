#######################################
# File name: emailer.py               #
# Author: Shrey Jaradi                #
# Created date : 7-2-2020             #
# Description : Use to send e-mail    #
#######################################

import yaml
import os
import smtplib 
from smtplib import SMTPAuthenticationError
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
import email.errors
import socket
import datetime
import re
import subprocess
import sys
import progressbar
from time import sleep

    
class SendMail:
    global descript
    descript =  """
To use this Package pass three dictionary parameter as follows: 
dt_credential   = {"email_account" : "<value>", 
                  "email_password" : "<value>"} 
dt_mail_setting = {"smtp_url" : "<value>",  
                  "smtp_port"   : <value>} 
dt_mail_data    = {"lst_to_addr" : ["<value>"],
                  "subject_lines" : "<value>", 
                  "mailbody" : "<value>", 
                  "mail_signature" : "<value>"}            
For example: 
#Here is a python program to help how to use this package: 
from emailer import emailer

eml = emailer.SendMail()
dt_credential   = {"email_account" : "example@outlook.com",
                   "email_password" : "example@123"} 
dt_mail_setting = {"smtp_url" : "smtp.office365.com", 
                   "smtp_port" : 587} 
dt_mail_data    = {"lst_to_addr" : ["example_one@gmail.com"],
                   "subject_lines" : "Package Use Example ", 
                   "mailbody" : "Please install package like this", 
                   "mail_signature" : "Thank you Example .com"}
#If you want to attach a file please pass another variable.
status = eml.send_email(dt_credential, dt_mail_setting, dt_mail_data)
print(status)
#for example : with attachment file 

attachment_file = "pic.png"  
status = eml.send_email(dt_credential, dt_mail_setting, dt_mail_data, attachment_file)
print(status) 
"""
    #__init__ method is constructor
    def __init__(self):
        print()
        
    #Mail Setting and formatting the mail body.    
    def __mail_config(self, username, to_addr, subject_line, mail_body, signature, attachment_file=None): 
        msg = ''
        try : 
            html_body = """<p>Hello,</br></br> %s</p>
            <p>Thank you 
            <br>%s""" %(mail_body, signature)
            regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$' 
            if(re.search(regex,to_addr)):         
                msg = MIMEMultipart()
                msg['Subject'] = "%s"%(subject_line)
                msg['From'] = username
                msg['To'] = to_addr
                # Attach HTML to the email
                body = MIMEText(html_body, 'html')
                msg.attach(body)
                # Attach Report to the email
                if attachment_file:
                    attachment = MIMEApplication(open(attachment_file, "rb").read())
                    attachment.add_header('Content-Disposition', 'attachment', filename=attachment_file)
                    msg.attach(attachment)
                return msg.as_string()
        except email.errors.MessageError as err:
            print("ERROR : {}".format())  
        except email.errors.CharsetError as err:
            print("ERROR : {}".format())
        except email.errors.MultipartInvariantViolationDefect as err:
            print("ERROR : {}".format())
        except email.errors.NoBoundaryInMultipartDefect as err:
            print("ERROR : {}".format())
        except email.errors.StartBoundaryNotFoundDefect as err:
            print("ERROR : {}".format())  
        except Exception as error:
            print("ERROR : %s" %(error))
        return msg          
    
    
    #Use to send mail and pass parameter in dictionary format with proper key and with attachment file.    
    def send_email(self, dt_credential, dt_mail_setting, dt_mail_data, attachment_file = None):
        """
        This Function is used to send_email. Pass three parameter in dictionary using below format.
        dt_credential  = {"email_account" : "<value>", 
                    "email_password" : "<value>"} 
        dt_mail_setting = {"smtp_url" : "<value>",  
                           "smtp_port"   : <value>} 
        dt_mail_data  = {"lst_to_addr" : ["<value>"],
                        "subject_lines" : "<value>", 
                        "mailbody" : "<value>", 
                        "mail_signature" : "<value>"}
        attachment_file = "example.png"                 
        """
        status = ""
        try : 
            if ((dt_credential == None) or (dt_mail_setting == None) or (dt_mail_data == None)):
                print("ERROR : Pass the Proper Parameter in Dictionary\n" + descript)
                exit()
            smtp_url =     dt_mail_setting['smtp_url']
            smtp_port =  dt_mail_setting['smtp_port']
            to_addr_lst =  dt_mail_data['lst_to_addr']
            subject_line =  dt_mail_data['subject_lines']
            mail_body =  dt_mail_data['mailbody']
            signature  =  dt_mail_data['mail_signature']
            username = dt_credential['email_account']
            password = dt_credential['email_password']            
            if ((username == None) or (password == None) or (to_addr_lst == None) or (smtp_url == None) or (smtp_port == None)):
                print("ERROR : Required values are empty please pass the value according to format \n" + descript)    
                exit()        
            smtp_details = smtp_url+":"+str(smtp_port)
            progress_bar = progressbar.ProgressBar(maxval=20, \
            widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
            progress_bar.start()
            for index, to_addr in enumerate(to_addr_lst):
                if attachment_file is not None:
                    msg = self.__mail_config(username, to_addr, subject_line, mail_body, signature, attachment_file) 
                else:
                    msg = self.__mail_config(username, to_addr, subject_line, mail_body, signature) 
                server = smtplib.SMTP(smtp_details)
                server.starttls()
                server.ehlo()
                server.login(username, password) 
                status = server.sendmail(username, to_addr, msg) 
                server.quit()
                progress_bar.update(index+1)
                print("INFO : Email successfully sent to %s." % to_addr)
                sleep(0.1)
            progress_bar.finish() 
            status = "Success"       
        except TimeoutError as err:
            print("ERROR : A connection attempt failed in Email_sender file.")
        except SMTPAuthenticationError:
            print("ERROR : Authentication Error")
        except KeyboardInterrupt as KeyboardInterruptError:
            print("ERROR : {}".format(KeyboardInterruptError)) 
        except InterruptedError as err:
            print("ERROR : {}".format(err))  
        except KeyError as err:
            print("ERROR : {} is missing".format(err))
            print("Please Check the below Example :\n" + descript)
        except ConnectionAbortedError as err:
            print("ERROR : {}".format(err))
        except ConnectionError as err:
            print("ERROR : {}".format(err))
        except ConnectionResetError as err:
            print("ERROR : {}".format(err))
        except ConnectionRefusedError as err:
            print("ERROR : {}".format(err))
        except smtplib.SMTPRecipientsRefused as err:
            print("ERROR : Invalid Mail Address")
        except smtplib.SMTPAuthenticationError as err:
            print("ERROR : Authentication Error")
        except smtplib.SMTPConnectError as err:
            print("ERROR : SMTP connection Error")
        except smtplib.SMTPServerDisconnected as err:
            print("ERROR : SMTP server not Connected")
        except smtplib.SMTPResponseException as err:
            print("ERROR : {}".format(err))
        except smtplib.SMTPException as err:
            print("ERROR : {}".format(err))
        except ValueError as err:
            print("ERROR : {}".format(err)) 
        except socket.error as err:
            print("ERROR : SMTP url or port number is wrong")
        except Exception as error:
            print("ERROR : {}".format(error))
        return status
            
        



