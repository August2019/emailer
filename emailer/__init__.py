#######################################
# File name: __init__.py              #
# Author: Shrey Jaradi                #
# Created date : 7-2-2020             #
# Description : Use to send e-mail    #
#######################################

import argparse 
from argparse import RawTextHelpFormatter
from emailer import emailer
from getpass import getpass
import sys

def main(): 
    try :
        descript = """To use this Package pass three dictionary parameter as follows: 
dt_credential   = {"email_account" : "<value>", 
                  "email_password" : "<value>"} 
dt_mail_setting = {"smtp_url" : "<value>",  
                  "smtp_port"   : <value>} 
dt_mail_data    = {"lst_to_addr" : ["<value>"],
                  "subject_lines" : "<value>", 
                  "mailbody" : "<value>", 
                  "mail_signature" : "<value>"}            
For example: 
#Here is a python program to help how to use this package: 
from emailer import emailer

eml = emailer.SendMail()
dt_credential   = {"email_account" : "example@outlook.com",
                   "email_password" : "example@123"} 
dt_mail_setting = {"smtp_url" : "smtp.office365.com", 
                   "smtp_port" : 587} 
dt_mail_data    = {"lst_to_addr" : ["example_one@gmail.com"],
                   "subject_lines" : "Package Use Example ", 
                   "mailbody" : "Please install package like this", 
                   "mail_signature" : "Thank you Example .com"}
#If you want to attach a file please pass another variable.
status = eml.send_email(dt_credential, dt_mail_setting, dt_mail_data)
print(status)
#for example : with attachment file 

attachment_file = "pic.png"  
status = eml.send_email(dt_credential, dt_mail_setting, dt_mail_data, attachment_file)
print(status) 
        """
        
        parser = argparse.ArgumentParser(prog ='emailer', 
                                        formatter_class = RawTextHelpFormatter,
                                        description ='Send Email Package \n' + descript) 
        parser.add_argument("--run","-r","-R", required=True,
                            action="store_true",
                            help="""To run the executable\n """)
        parser.parse_args(args=None if sys.argv[1:] else ['--help'])
        args = parser.parse_args()
        if args.run:
            dt_credential, dt_mail_setting, dt_mail_data =  get_details()
            confirmation = input("Do you want to really want to proceed and send the mail ?(y/n)")
            if confirmation == 'y' :    
                eml = emailer.SendMail()
                eml.send_email(dt_credential, dt_mail_setting, dt_mail_data)
            else:
                print("Exiting !!!!")
                exit()
        else:
            parser.print_help()
    except Exception as error :
        print("ERROR : {}".format(error))
    except KeyboardInterrupt as KeyboardInterruptError:
        print("ERROR : {}".format(KeyboardInterruptError))
    except InterruptedError as err:
        print("ERROR : {}".format(err))

def get_details():
    dt_credential = ""
    dt_mail_setting = ""
    dt_mail_data = ""
    try : 
        print("Answer the Following Question : \n")
        email_account =  input("What is your username/email ?\n")
        email_password = getpass("What is your Email Password ?:\n")
        host_url =  input("What is the SMTP URL ?\n") 
        host_port =  input("What is the SMTP port ?\n") 
        to_addr = input("To whom you want to send the mails ? (Note : If more than one then put it in comma without space)\n")
        lst_to_addr = to_addr.split (",")
        subject_lines = input("Write the subject line of mail ?\n")
        mail_body = input("Write the Mail Body ?\n")
        signature = input("Write the Mail Signature ?\n")
        dt_credential   = {"email_account" : email_account, 
                            "email_password" : email_password} 
        dt_mail_setting = {"smtp_url" : host_url,  
                            "smtp_port" : host_port} 
        dt_mail_data    = {"lst_to_addr" : lst_to_addr, 
                            "subject_lines" : subject_lines, 
                            "mailbody" : mail_body, 
                            "mail_signature" : signature} 
    except Exception as error :
        print("ERROR : {}".format(error))
    except KeyboardInterrupt as KeyboardInterruptError:
        print("ERROR : {}".format(KeyboardInterruptError))
    except InterruptedError as err:
        print("ERROR : {}".format(err))
    return dt_credential, dt_mail_setting, dt_mail_data
        
