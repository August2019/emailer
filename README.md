----------------------
Command Line Arguments
----------------------

optional arguments:
  -h, --help     show this help message and exit
  --run, -r, -R  To run the executable
For example :
 >emailer --run  #This will run the Executable 
 >emailer --help #This will help how to use the package 
------------------------
How to Use this Package
------------------------
User can Copy this code and paste it to your code to Use this package.

For example  :

#Here is a python program to help how to use this package: 
from emailer import emailer

eml = emailer.SendMail()
dt_credential   = {"email_account" : "example@outlook.com",
                   "email_password" : "example@123"} 
dt_mail_setting = {"smtp_url" : "smtp.office365.com", 
                   "smtp_port" : 587} 
dt_mail_data    = {"lst_to_addr" : ["example_one@gmail.com"],
                   "subject_lines" : "Package Use Example ", 
                   "mailbody" : "Please install package like this", 
                   "mail_signature" : "Thank you Example .com"}
#If you want to attach a file please pass another variable.
status = eml.send_email(dt_credential, dt_mail_setting, dt_mail_data)
print(status)
#for example : with attachment file 

attachment_file = "pic.png"  
status = eml.send_email(dt_credential, dt_mail_setting, dt_mail_data, attachment_file)
print(status) 